def add(a,b):
    return a+b

def mult(a,b):
    return a*b

if __name__ == '__main__':
    a,b = 2,3
    res = add(a,b)
    new_res = mult(res,2)
    print(new_res)