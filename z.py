import boto3

db = boto3.resource('dynamodb', region_name='us-east-1', aws_access_key_id="tmp",
    aws_secret_access_key="tmp", endpoint_url='http://localhost:8000') 

dbc = boto3.client('dynamodb', region_name='us-east-1', aws_access_key_id="tmp",
    aws_secret_access_key="tmp", endpoint_url='http://localhost:8000') 

def create_table():
    db.create_table(
        TableName='data_sets',
        KeySchema=[
            {
                'AttributeName': 'model_name',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'model_name',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

def add():
    table = db.Table("data_sets")
    table.put_item(
        Item = {
            'model_name' :'nlp',
            'file_name':'test.xlsx'
        }
    )

def remove():
    table = db.Table("data_sets")
    table.delete()

def get():
    table = db.Table("data_sets")
    resp = table.get_item(
        Key={
            'model_name':'nlp'
        })
    print(resp)

def test():
    table = db.tables
    print(table)

def list_tables():
    print(dbc.list_tables())

def show_data():
    print(dbc.scan(TableName='data_sets'))
    # print(dbc.describe_table(TableName='models'))
    # t = db.Table('models')
    # print(t.get_available_subresources())

# create_table()
# add()
# remove()
list_tables()
# show_data()
# get()
# test()