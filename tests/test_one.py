import unittest
import one.main

class Test(unittest.TestCase):
    def test_add(self):
        self.assertEqual(one.main.add(2,3),5)

    def test_add_negative(self):
        self.assertEqual(one.main.add(-2,-5),-7)

if __name__=='__main__':
    unittest.main()